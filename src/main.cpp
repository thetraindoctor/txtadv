#include <iostream>
#include <string>
#include <chrono>
#include <thread>
#include <ncurses.h>

using namespace std;

int key;    
int mx_x = 27, mx_y = 9;   //maximum bounds
int mn_x = 0, mn_y = 0;   //minimum x and y
bool spawned = false;
char player = '@';

string xpo, ypo;

int xpos, ypos, xprev, yprev;   //initial position
char rogue_char;   //player char
WINDOW *rogue_win, *console_win, *info_win, *menu_win;

string get_pos(char axis) {
    if(axis == 'x') return "xpos: " + to_string(getcurx(rogue_win));
    if(axis == 'y') return "ypos: " + to_string(getcury(rogue_win));
    else return "ERROR";
}

void print_pos(char axis, int y, int x) {
    if(axis == 'x') mvwaddstr(console_win, y, x, xpo.c_str());
    if(axis == 'y') mvwaddstr(console_win, y, x, ypo.c_str());
}

int main(int argc, char *argv[]) {

    initscr();
    raw();
    keypad(stdscr, TRUE);
    noecho();
    cbreak();
    curs_set(0);

    rogue_win = newwin(mx_y, mx_x, 0, 0);
    console_win = newwin(27, 54, mx_y+1, 0);

    rogue_char = player;
    ypos = 1;
    xpos = 1;

    // Game loop 
    for(;;){
        if(spawned == false) {
            wborder(rogue_win, '#', '#', '=', '=', '%', '%', '%', '%'); 
            mvwaddch(rogue_win, ypos, xpos, rogue_char);
            xpo = get_pos('x');
            ypo = get_pos('y');
            print_pos('y', 1, 1);
            print_pos('x', 2, 1);
            wrefresh(console_win);
            wrefresh(rogue_win);
            spawned = true;
        }

        /*key = wgetch(rogue_win);

        switch(key){
            case KEY_LEFT:
                xpos -= 1;
                mvwaddch(rogue_win, ypos, xpos+1, ' ');
                break;
            case KEY_RIGHT:
                xpos += 1;
                mvwaddch(rogue_win, ypos, xpos-1, ' ');
                break;
            case KEY_UP:
                ypos -= 1;
                mvwaddch(rogue_win, ypos+1, xpos, ' ');
                break;
            case KEY_DOWN:
                ypos += 1;
                mvwaddch(rogue_win, ypos-1, xpos, ' ');
                break;
        }

        ungetch(key);

        if(xpos+1 >= mx_x) {
            xpos = mx_x-1;
        }
        if(xpos <= mn_x) {
            xpos = mn_x+1;
        }

        if(ypos+1 >= mx_y) {
            ypos = mx_y-1;
        }
        if(ypos <= mn_y) {
            ypos = mn_y + 1;
        }*/

        xpo = get_pos('x');
        ypo = get_pos('y');
        print_pos('y', 1, 1);
        print_pos('x', 2, 1);

        mvwaddch(rogue_win, ypos, xpos, rogue_char);
        wborder(rogue_win, '#', '#', '=', '=', '%', '%', '%', '%'); 
        wrefresh(rogue_win);
        wrefresh(console_win);
    }

    // End ncurses mode and exit program
    endwin();
    return 0;
}